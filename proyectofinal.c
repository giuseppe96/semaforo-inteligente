#include <stdio.h>	//standar input/output definitions
#include <stdlib.h>	//funcion de conversion de cadenas
#include <unistd.h>	//UNIX standar funcion definitions
#include <string.h>	//funcion de manipulacion de cadenas
#include <fcntl.h>	//file control definitions
#include "termset.h"

#define N 60

int obtener_archivo(FILE *fptr);
int leer_censo(FILE *fptr);
int carga_intervalo(int argc, char *argv[]); 

// const char port[] = "/dev/ttyUSB0";
 const int baudrate = 9600;

 
    int fd;              // file decriptor
    int ch;
    FILE * fptr;         //puntero al puerto, "r", osea para lectura 
    FILE * fdout;        //puntero al archivo de salida
    ssize_t n_read;      //bytes leidos
    size_t len ;         //numero de bytes
    char line[N];        //linea de texto a copiar
 
 int flag = 1;
 
int main(int argc, char *argv[])
 {

int op;
FILE *fptr;

do{
	
	printf("\n OPCIONES \n");
	printf("1) generar archivo de texto\n");
	printf("2) leer datos\n");
	printf("3) cargar nuevos intervalos en los semaforos\n");
	printf("4) salir\n ");
	printf("ingrese una opcion (numero): ");		scanf("%d", &op);

switch(op)
{	
	case 1:
		obtener_archivo(fptr);		//genero el archivode texto con lo que me llega del puerto serie
		
		break;
	case 2:
		leer_censo(fptr);		//obtengo la cantidad de pulsaciones					    						   de cada sensor
		break;
	case 3:
		carga_intervalo(argc,argv);		//cargo el intervalo de delay en los semaforos 
		break;

	case 4:
		flag = 0;
		break;
	default:
		printf("\nopcion invalida\n");
		break;
}
}while(flag);

return 0;
}			//fin de funcion main

int obtener_archivo(FILE *fptr)
{
   const char port[] = "/dev/ttyUSB0";
   const int baudrate = 9600;

/*
   int fd;		  // file decriptor
   int ch;
    // FILE * fptr;         //puntero al puerto, "r", osea para lectura 
     FILE * fdout;        //puntero al archivo de salida
   ssize_t n_read;      //bytes leidos
   size_t len ;    	  //numero de bytes
   char line[N];        //linea de texto a copiar
*/
 /* Apertura de puerto serie y archivo de salida Ubicaciones.txt */


   fptr = fopen (port, "r");	//abro el archivo con el nombre del arreglo "puerto" en formato de lectura
   fdout = fopen ("DATOS.txt", "w" ); //abrimos el archivo de salida en formato de escritura
 
 
   /* Configuración del puerto serie **/
 
   if(fptr == NULL)		//en caso de que no se reciba informacion del puerto...
   {
     printf("ERROR: no se pudo abrir el dispositivo\n");
     return -1;
   }
 
   if(fdout == NULL)		//en caso de que no se reciba informacion para crear el archivo .txt...
   {
     printf("ERROR: no se pudo abrir el archivo de salida\n");
     return -1;
   }
 
 
   fd = fileno(fptr);		//le asigno al file descriptor 
 

 if(termset(fd, baudrate, &ttyold, &ttynew) == -1)	//en caso de que la configuracion del termset este mal...
   {
     printf("ERROR: configuración de tty\n");
     return -1;
}
 
   /** Copia de las lineas obtenidas del puerto serie al archivo DATOS.txt **/
 
     while (fgets(line, N, fptr) ){	//mientras haya lineas por copiar provenientes del puerto serie...
             puts(line);		//
       fputs(line, fdout);		//las redirijo al archivo.txt de salida

     }

   fclose(fptr);		//cierro el archivo de entrada
   fclose(fdout);		// ""	""	""	salida
   flag=0;
   return EXIT_SUCCESS;
 }

//AHORA DEBO ABRIR EL ARCHIVO Y RECORRER CADA CADENA DE CARACTERES DESDE "inicio de censo" HASTA "fin de censo"

int  leer_censo (FILE *fptr){

 
fptr = fopen ("DATOS.txt", "r");

if(fptr == NULL)             //en caso de que no se reciba informacion del puerto...
    {
      printf("ERROR: no se pudo abrir el dispositivo\n");
      return -1;
    }

 if(termset(fd, baudrate, &ttyold, &ttynew) == -1)      //en caso de que la configuracion del termset este mal...
    {
      printf("ERROR: configuración de tty\n");
      return -1;
    }

 while (fgets(line, N, fptr) ){     //mientras haya lineas por copiar provenientes del puerto serie...
        puts(line);    		    // produce un bucle indefinido 
/*
for (;;)
{
n_read = getline (&line, &len, fptr);

if (n_read != -1)

	printf("Line :%s ", line);
}
*/
fclose ( fptr );
return EXIT_SUCCESS;
}
}
//LA SIGUIENTE FUNCION ME ENVIA UN NUMERO DE DELAY, PARA SALIR SE TECLEA "q"

int carga_intervalo(int argc, char *argv[])
{
unsigned char byte = 0;
struct termios oldtty, newtty;

if (argc != 2)
	printf("USAR: %s <device>\n", argv[0]);

fd = open(argv[1], O_RDWR | O_NOCTTY | O_NDELAY);
if(fd ==-1)
{
	printf("error, no se pudo abrir el dispositivo.\n");
	return -1;
}
termset(fd,9600, &oldtty, &newtty);

for(;;){

scanf(" %c", &byte);

if(byte=='q')
	return 0;

write(fd, &byte, 1);
}
return 0;
}
