#include <RTClib.h>
#include <Wire.h>

#define PULSADOR 8

//  (formato  de 24hs)
//  INICIO DE FRANJAS:

unsigned char f[] = { 7, 00, 00,        //inicio de 1er franja horaria
                     12, 00, 00,        //  "     " 2da     "       "                     
                     17, 00, 00,        //  "     " 3er     "       "
                     21, 00, 00};       //  "     " 4ta     "       "

//  FIN DE FRANJAS:
unsigned char fin_f[]={ 9, 30, 00,      //fin de 1er franja horaria
                       14, 30, 00,      // "  "   2da "         "               
                       17, 00, 15,      // "  "   3ra "         "
                       22, 30, 00};     // "  "   4ta "         "
    

RTC_DS3231 rtc;

int i=0;

void setup() 
{
    Serial.begin(9600);
    
    if(!rtc.begin()){
    
    Serial.println ("modulo RTC no encontrado!");
    
    while(1);
    }
    rtc.adjust (DateTime(2019, 11, 20, 16, 59, 55)); 
//  rtc.adjust (DateTime(__DATE__), (__TIME));        ajusta el reloj a fecha y hora actual
  
 //if (rtc.lostPower()) {                                     por si se llegara a acabar la bateria del modulo pero no tiene
 //                                                           sentido si no esta puesto el horario actual...
  //    Serial.println("RTC lost power, lets set the time!");
  //    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

    pinMode(PULSADOR, INPUT);

}

void loop() 
{
  DateTime fecha;
   /*Hacer mientras no sea hora de censado*/ 
  do {
    
       fecha = rtc.now();
     //Serial.println("Primer while");        //debug mode: "off"
       imprimir_fecha();
          
    }while(!(fecha.hour() == f[6] && fecha.minute()== f[7] && fecha.second() == f[8] ));  //El censado comienza a las 17:00:00 hs. 
  i=0;
        Serial.print("inicio de censo\n");
  do {
    
        fecha = rtc.now();
       //Serial.println("Segundo while");    //  debug mode: "off"
      //  imprimir_fecha();                     en caso de que quiera ver el horario de las pulsaciones descomentar
          
        if(digitalRead(PULSADOR) == HIGH) {
            
             delay(200);                            //Para el efecto rebote del pulsador. 
              i++;
              Serial.println(i);
       }
    
    }while(!(fecha.hour() == fin_f[6] && fecha.minute()==fin_f[7] && fecha.second() == fin_f[8]) );

  if((fecha.hour() == fin_f[6] && fecha.minute()==fin_f[7] && fecha.second() == fin_f[8])){

      Serial.print("finalizo el censo\n");
  }
}

//FUNCIONES:
//*********

void imprimir_fecha()
{
DateTime fecha = rtc.now();
    
    Serial.print(fecha.day());
    Serial.print("/");
    Serial.print(fecha.month());
    Serial.print("/");
    Serial.print(fecha.year());
    Serial.print("  ");
    Serial.print(fecha.hour());
    Serial.print(":");
    Serial.print(fecha.minute());
    Serial.print(":");
    Serial.print(fecha.second());
    Serial.print(" \n ");
    delay(1000);
}
